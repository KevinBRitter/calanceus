﻿using System;

namespace Ritter_API_Solution
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            APITool gogetter = new APITool();
            // Try another repo here if you want.  The tool adds the commit bit on the end.
            gogetter.PullCommitMessages("https://api.bitbucket.org/2.0/repositories/calanceus/interviews17");
            gogetter.Unpack();
            gogetter.WriteToFile("output2.txt");
            Console.WriteLine("Goodbye World!");
        }
    }
}
