﻿using System;
using System.Text.Json;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.IO;

namespace Ritter_API_Solution
{
    class APITool
    {
        /*
         * While I have used C# in several courses, I had not once tried to publish an application
         * to an executable, or pulled data from an API.  I have used string builder and file before 
         * but I don't remember working with dictionaries in C# as I tried to use them here, and certainly
         * have not worked with the webclient type at all.  My solution is a little hokey in the unpack method 
         * and I know I didn't need to call the methods separately from main either.  I try to live by the 
         * engineering principle of "Make it work.  Then, if there's time, make it pretty."
         */
        WebClient clientRequest = new WebClient();
        string data;
        StringBuilder outputString = new StringBuilder();
        public APITool() { }
        public void PullCommitMessages(string urlOfRepo)
        {
            // I split the string up incase someone wanted to send another repo in to test.
            string commitSelect = urlOfRepo + "/commits?fields=values.message,values.date";
            // The way the client request method 'download string' works made me think my python
            // solution pulling a byte object wasn't necessary and I just needed to look for a string option.
            this.data = this.clientRequest.DownloadString(commitSelect);
            Console.Out.WriteLine(data);
        }
        public void Unpack() {
            // I had a very fun/ confusing time trying to match the nested data types here for deserialization.
            // I'm absolutely positive this isn't the best way to do this but it works <-- famous last words.
            Dictionary<string, Dictionary<string, string>[]> localDict = JsonSerializer.Deserialize<Dictionary<string, Dictionary<string, string>[]>>(this.data);
            Console.Out.WriteLine();
            Dictionary<string, string>[] myList = localDict["values"];
            // Not a fan of nested for loops either.  This is an example of where to start refactoring for next time.
            foreach (Dictionary<string, string> someDict in myList) {
                foreach (KeyValuePair<string, string> pair in someDict) {
                    this.outputString.Append(pair.Key + ": " + pair.Value + ", ");
                }
                this.outputString.Append(Environment.NewLine);
            }
            Console.Out.WriteLine(this.outputString);
        }
        public void WriteToFile(string fileLocation) {
            File.WriteAllText(fileLocation, this.outputString.ToString());
        }
    }
}